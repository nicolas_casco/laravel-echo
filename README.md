#v 2.0

Clone this repository (or download and extract the .zip)
Run composer install from inside the project directory to download PHP dependencies
Run npm install or yarn to download JS dependencies
Run cp .env.example .env to create your projects' .env file
Run php artisan key:generate to create a new encryption key
Open the project and edit the .env file to add database settings to your project. Take note of the database name, password, and username to make sure they match your system's settings. Change any other environment settings you desire, but no other setting changes are required to follow this tutorial.
Back in the terminal, run php artisan migrate to migrate your database
Run php artisan db:seed to seed your database

composer require pusher/pusher-php-server
npm install --save pusher-js
npm install --save laravel-echo

config/app.php enable broadcast

broadcasting.php : 'pusher' => [
            'driver' => 'pusher',
            'key' => env('PUSHER_APP_KEY'),
            'secret' => env('PUSHER_APP_SECRET'),
            'app_id' => env('PUSHER_APP_ID'),
            'options' => [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'ecrypted' => true
            ],
        ]

.env

csrf token en todas las paginas

Bootstrap.js enable echo:
import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '6003cfa8b27251065fcc',
    cluster: 'us2',
    encrypted: true
});


npm run dev

php artisan make:event NewComment

class NewComment implements ShouldBroadcastNow

NewComment.php
CommentController.php => broadcast(new NewComment($comment))->toOthers();
